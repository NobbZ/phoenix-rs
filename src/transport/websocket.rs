use websocket::client::builder::{ClientBuilder, Url};
use websocket::client::sync::Client;
use websocket::stream::sync::TcpStream;

use transport::Transport;


pub struct Websocket {
    client: Client<TcpStream>,
}

impl Transport for Websocket {
    fn new(url: &str) -> Websocket {
        let parsed = Url::parse(url).unwrap();

        let client = match parsed.scheme() {
            "ws" => ClientBuilder::new(url).unwrap().connect_insecure().unwrap(),
            _ => panic!("unsupported schema"),
        };

        Websocket { client: client }
    }

    fn close(&mut self) {
        self.client.shutdown().unwrap();
    }
}
