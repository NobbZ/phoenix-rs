mod websocket;

pub trait Transport {
    fn new(url: &str) -> Self;

    fn close(&mut self);
}
